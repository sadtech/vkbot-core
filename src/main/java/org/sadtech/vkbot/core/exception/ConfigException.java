package org.sadtech.vkbot.core.exception;

public class ConfigException extends RuntimeException {

    public ConfigException(String message) {
        super(message);
    }
}
